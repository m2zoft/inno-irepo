<?php

namespace Inno\IRepo;
use Illuminate\Support\ServiceProvider;
use Inno\IRepo\Console\Command\RepositoryMakeCommand;

class IRepoServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->runningInConsole()) {
	        $this->commands([
	            RepositoryMakeCommand::class,
	        ]);
	    }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
